import Image from "next/image";

const Footer = () => {

  return (
    <div className="bg-light-gray">

      <div className="container mx-auto max-w-screen-lg pt-8 pb-4 px-6">

        <div className="grid sm:grid-cols-5 grid-cols-2 gap-4 border mb-4 pb-6 border-t-0 border-l-0 border-r-0">
          <div>
            <h6 className="mb-4 text-sm text-gray-500">Creators</h6>
            <ul>
              <li className="mb-2"><a href="https://zeno.fm/streaming/">Radio Streaming</a></li>
              <li className="mb-2"><a href="https://zeno.fm/podcasting/">Podcast</a></li>
              <li className="mb-2"><a href="https://zeno.fm/video/">Video</a></li>
              <li className="mb-2"><a href="https://zeno.fm/donations/">Donations</a></li>
              <li className="mb-2"><a href="https://zeno.fm/call-to-listen/">Call-To-Listen</a></li>
              <li className="mb-2"><a href="https://zeno.fm/prayer-lines/">Prayer lines</a></li>
              <li className="mb-2"><a href="https://zeno.fm/merch/">Merch</a></li>
              <li className="mb-2"><a href="https://zeno.fm/monetization/">Monetization</a></li>
              <li className="mb-2"><a href="https://zeno.fm/landing/">Landing page</a></li>
            </ul>
          </div>
          <div>
            <h6 className="mb-4 text-sm text-gray-500">Community</h6>
            <ul>
              <li className="mb-2"><a href="https://zeno.fm/long-distance-calling/">Long distance calling</a></li>
              <li className="mb-2"><a href="https://zeno.fm/top-up/">Top up</a></li>
              <li className="mb-2"><a href="https://zeno.fm/offers/">Offers</a></li>
              <li className="mb-2"><a href="https://zenoplus.co/" target="_blank" rel="noopener">Zeno+ App</a></li>
            </ul>
          </div>
          <div>
            <h6 className="mb-4 text-sm text-gray-500">Partner With Us</h6>
            <ul>
              <li className="mb-2"><a href="https://zeno.fm/advertise/">Advertise</a></li>
              <li className="mb-2"><a href="https://zeno.fm/partners-aggregators/">Aggregators</a></li>
              <li className="mb-2"><a href="https://zeno.fm/app-network/">App Network</a></li>
            </ul>
          </div>
          <div>
            <h6 className="mb-4 text-sm text-gray-500">About us</h6>
            <ul>
              <li className="mb-2"><a href="https://zeno.fm/about-us/">What we do</a></li>
              <li className="mb-2"><a href="https://zeno.fm/about-us/press/">Press</a></li>
              <li className="mb-2"><a href="https://zeno.fm/about-us/team/">Team</a></li>
              <li className="mb-2"><a href="https://zeno.fm/contact-us/">Contact</a></li>
            </ul>
          </div>

          <div>
            <h6 className="mb-4 text-sm text-gray-500">Follow us on social media!</h6>
            <ul>
              <li className="mb-2"><a href="https://zeno.fm/about-us/">Facebook</a></li>
              <li className="mb-2"><a href="https://zeno.fm/about-us/press/">Press</a></li>
              <li className="mb-2"><a href="https://zeno.fm/about-us/team/">Team</a></li>
              <li className="mb-2"><a href="https://zeno.fm/contact-us/">Contact</a></li>
            </ul>
          </div>
        </div>


        <div className="flex flex-wrap flex-row items-center text-sm">

          <div className="flex flex-row items-center sm:mx-0 mx-auto sm:mb-0 mb-6">
            <Image
              src="/images/logo.png"
              alt="Zeno FM"
              width={48}
              height={48}
            />
            <div className="ml-4">© 2021 Zeno FM. All rights reserved.</div>
          </div>

          <div className="sm:ml-auto sm:mr-0 mx-auto">
            <ul className="flex">
              <li className="mr-4">
                <a href="https://www.zenomedia.com/wp-content/themes/zeno/privacy_policy.pdf"
                   target="_blank" rel="noopener">
                  Privacy
                </a>
              </li>
              <li className="mr-4">
                <a href="https://www.zenomedia.com/wp-content/themes/zeno/zeno_terms.pdf"
                   target="_blank" rel="noopener">
                  Terms
                </a>
              </li>
              <li className="mr-4"><a href="https://zeno.fm/cookies_policy.pdf">Cookies</a></li>
              <li className="mb-2"><a href="https://zeno.fm/gdpr-privacy-policy/">GDPR</a></li>
            </ul>
          </div>


        </div>

      </div>

    </div>
  )

}

export default Footer