import {FiPauseCircle, FiPlayCircle} from "react-icons/fi";
import React from "react";

const EpisodeList = ({episodes, player, onPlay, onPause}) => {
  return (
    <div className="relative container max-w-screen-lg mx-auto text-white px-4">

      <div className="sm:overflow-y-auto overflow-auto sm:h-96 h-auto">
        {episodes.map((episode) => (
          <div key={episode.key} className="my-2 pb-2 border-0 border-b border-white border-opacity-25">
            <div className="flex flex-row items-center">
              {!player.episode || player.episode.key !== episode.key || (player.episode.key === episode.key && !player.playing)
                ? <button onClick={() => onPlay(episode)} className="focus:outline-none">
                  <FiPlayCircle className="text-3xl"/>
                </button>
                : <button onClick={() => onPause(episode)} className="focus:outline-none">
                  <FiPauseCircle className="text-3xl"/>
                </button>
              }

              <div className="ml-2">
                <h4 className="text-lg">
                  {episode.title}
                </h4>

                <div className="text-sm text-white text-opacity-70">{episode.publish_date}, {episode.duration_hms} </div>
              </div>
            </div>

            {
              episode.description !== '-' &&
                <div className="mt-2 text-xs break-words">{episode.description}</div>
            }

          </div>
        ))}
      </div>

    </div>
  )
}

export default EpisodeList