import React from "react";

const CardAnnouncements = ({station, card}) => {

  const getPrettyDate = (unixtime) => {
    let date = new Date(unixtime * 1000)

    const options = { year: 'numeric', month: 'long', day: 'numeric', hour:'2-digit', minute: '2-digit' };

    return date.toLocaleDateString('en-US', options)
  }

  return (
    <div className="shadow-md bg-white rounded p-6 mb-6 break-inside">
      <h2 className="text-xl font-medium mb-4">Announcements</h2>

      {card.announcements.map((announcement, index) => (

        <article className={'border-0 py-4' + (index < 2 ? ' border-b' : '')} key={announcement.id}>
          <h5 className="mb-2 font-semibold">{announcement.title}</h5>
          <p className="mb-2">{announcement.content}</p>
          <div className="text-gray-400 font-semibold text-sm">{ getPrettyDate(announcement.publishDate) }</div>
        </article>

      ))}

    </div>
  )

}

export default CardAnnouncements