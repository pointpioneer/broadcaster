import React from "react";
import Image from "next/image";
import {FiGlobe, FiHash, FiMapPin} from "react-icons/fi";

const CardAbout = ({station, card}) => {

  return (
    <div className="shadow-md bg-white rounded p-6 mb-6">

      <div>
        <div className="text-2xl mb-4">About the station</div>

        <div className="flex flex-row flex-wrap items-center text-sm mb-2">

          {
            station.category.length > 0 &&
            <a href="" className="flex flex-row items-center mr-6 hover:text-primary">
              <FiHash /> <span className="ml-1 font-semibold">{station.category}</span>
            </a>
          }

          <a href="" className="flex flex-row items-center mr-6 hover:text-primary">
            <FiMapPin /> <span className="ml-1 font-semibold">{station.city ? station.city + ', ' : ''} {station.country}</span>
          </a>

          {station.languages.length > 0 &&
          <a href="" className="flex flex-row items-center hover:text-primary">
            <FiGlobe/> <span className="ml-1 font-semibold">{station.languages.join(', ')}</span>
          </a>
          }
        </div>

        <div className="break-words" dangerouslySetInnerHTML={{
          __html: card.text
        }}/>
      </div>

    </div>
  )

}

export default CardAbout