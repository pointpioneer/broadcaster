import {FaFacebook, FaSnapchatGhost, FaSpotify, FaTwitter, FaYoutube} from "react-icons/fa";
import { AiFillInstagram } from "react-icons/ai";

const CardSocialMedia = ({station, stationCards}) => {

  return (
    <div className="shadow-md bg-white rounded p-6 mb-6 break-inside">
      <h2 className="text-xl font-medium mb-4">Follow us</h2>

      <p className="mb-2">Let’s follow our station on social media!</p>

      <ul className="follow-list">

        {
          typeof stationCards.CardSocialMedia.facebookUrl != "undefined" &&
          <li>
            <a href={stationCards.CardSocialMedia.facebookUrl} target="_blank" rel="noopener"
               className="transition-color flex flex-row items-center py-3 border-0 border-b hover:text-primary">
                    <span className="follow-icon mr-2">
                      <FaFacebook/>
                    </span>
              Follow on Facebook
            </a>
          </li>
        }

        {
          typeof stationCards.CardSocialMedia.twitterUrl != "undefined" &&
          <li>
            <a href={stationCards.CardSocialMedia.twitterUrl} target="_blank" rel="noopener"
               className="transition-color flex flex-row items-center py-3 border-0 border-b hover:text-primary">
                    <span className="follow-icon mr-2">
                        <FaTwitter/>
                    </span>
              Follow on Twitter
            </a>
          </li>
        }

        {
          typeof stationCards.CardSocialMedia.instagramUrl != "undefined" &&
          <li>
            <a href={stationCards.CardSocialMedia.instagramUrl} target="_blank" rel="noopener"
               className="transition-color flex flex-row items-center py-3 border-0 border-b hover:text-primary">
                    <span className="follow-icon mr-2">
                        <AiFillInstagram/>
                    </span>
              Follow on Instagram
            </a>
          </li>
        }

        {
          typeof stationCards.CardSocialMedia.snapchatUrl != "undefined" &&
          <li>
            <a href={stationCards.CardSocialMedia.snapchatUrl} target="_blank" rel="noopener"
               className="transition-color flex flex-row items-center py-3 border-0 border-b hover:text-primary">
                    <span className="follow-icon mr-2">
                        <FaSnapchatGhost/>
                    </span>
              Follow on Snapchat
            </a>
          </li>
        }

        {
          typeof stationCards.CardSocialMedia.youtubeUrl != "undefined" &&
          <li>
            <a href={stationCards.CardSocialMedia.youtubeUrl} target="_blank" rel="noopener"
               className="transition-color flex flex-row items-center py-3 border-0 border-b hover:text-primary">
                    <span className="follow-icon mr-2">
                        <FaYoutube/>
                    </span>
              Follow on Youtube
            </a>
          </li>
        }

        {
          typeof stationCards.CardSocialMedia.spotifyUrl != "undefined" &&
          <li>
            <a href={stationCards.CardSocialMedia.spotifyUrl} target="_blank" rel="noopener"
               className="transition-color flex flex-row items-center py-3 border-0 border-b hover:text-primary">
                    <span className="follow-icon mr-2">
                        <FaSpotify/>
                    </span>
              Follow on Spotify
            </a>
          </li>
        }

      </ul>
    </div>
  )

}

export default CardSocialMedia