const CardSupportDonations = ({station, stationCards}) => {

  return (
    <div className="shadow-md bg-white rounded p-6 mb-6 break-inside">
      <h2 className="text-xl font-medium mb-4">Support Your Broadcaster</h2>

      <p>As you know, we are an independent broadcast station. For us, It has always been hard to maintain our broadcast without your support.</p>
      <br/>
      <p>To support us, please click to the donate button and then choose the app you’ll donate with. Thank you very much!</p>

      {
        typeof stationCards.CardSupportDonations.stripeData != "undefined" &&
        <a className="block my-4 py-3 text-center px-6 rounded-full font-bold bg-stripe text-white bg-green bg-opacity-90 hover:bg-opacity-100"
           href={`https://editor.zenomedia.com/resources/stripe/donate.html?station_key=${station.key}`} target="_blank" rel="noopener">
          Donate with Stripe
        </a>
      }

      {
        typeof stationCards.CardSupportDonations.paypalUrl != "undefined" &&
        <a className="block my-4 py-3 text-center px-6 rounded-full font-bold bg-paypal text-white bg-opacity-90 hover:bg-opacity-100"
           href={stationCards.CardSupportDonations.paypalUrl} target="_blank" rel="noopener">
          Donate with PayPal
        </a>
      }

      {
        typeof stationCards.CardSupportDonations.cashappUrl != "undefined" &&
        <a className="block my-4 py-3 text-center px-6 rounded-full font-bold bg-primary bg-cashapp text-white bg-opacity-90 hover:bg-opacity-100"
           href={stationCards.CardSupportDonations.cashappUrl} target="_blank" rel="noopener">
          Donate with Cash App
        </a>
      }
    </div>
  )

}

export default CardSupportDonations