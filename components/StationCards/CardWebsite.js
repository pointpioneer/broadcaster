import React from "react";
import Image from "next/image";

const CardWebsite = ({station, card}) => {

  return (
    <div className="shadow-md bg-white rounded py-6 mb-6 break-inside">
      <h2 className="text-xl font-medium mb-4 px-6">{card.name}</h2>

      {
        typeof card.image != "undefined" &&
          <Image src={card.image} height={320} width={320} layout="responsive" alt={card.name}/>
      }

      <div className="mt-4 mx-6">
        <p className="mb-8">Visit our website to get notified latest news and updates.</p>

        <a className="block py-3 text-center px-6 rounded-full font-bold bg-primary bg-opacity-90 hover:bg-opacity-100" href={card.url} target="_blank" rel="noopener">
          Visit our website
        </a>
      </div>
    </div>
  )

}

export default CardWebsite