import React from "react";
import Image from "next/image";
import {FaApple, FaGooglePlay} from "react-icons/fa";

const CardAppStore = ({station, card}) => {

  return (
    <div className="shadow-md bg-black text-white rounded p-6 mb-6 break-inside">

      <div>
        <h2 className="text-xl font-medium text-center">Get the app for free</h2>

        <div className="p-8 text-center">
          <Image src={card.qrCodeUrl} className="rounded" height={200} width={200} alt={card.name}/>
        </div>
      </div>

      <div className="flex text-sm">
        <a href={card.appLinkApple} className="flex flex-row flex-1 mr-1 items-center py-2 rounded bg-app hover:bg-opacity-80">
          <span className="flex mx-auto">
            <span className="text-3xl mr-1">
              <FaApple/>
            </span>

            <span>
              <span className="block text-xs">Available on the</span>
              <span>App Store</span>
            </span>
          </span>
        </a>

        <a href={card.appLinkGoogle} className="flex flex-row flex-1 ml-1 items-center py-2 rounded bg-app hover:bg-opacity-80">
          <span className="flex mx-auto">
            <span className="text-3xl mr-1">
              <FaGooglePlay />
            </span>

            <span>
              <span className="block text-xs">Get it on</span>
              <span>Goggle Play</span>
            </span>
          </span>
        </a>
      </div>
    </div>
  )

}

export default CardAppStore