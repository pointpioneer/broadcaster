import Image from "next/image";
import {FiPause, FiPlay, FiShare2} from "react-icons/fi";
import ShareModal from "./ShareModal";

const StationInfo = ({station, player, onPlay, onPause, nowPlaying}) => {

  return (
    <div className="relative container max-w-screen-lg mx-auto py-7 text-white">
      <h1 className="text-center text-3xl mb-8 text-white">
        Station {station.name}
      </h1>

      <div className="text-center">

        <div id="stationImageContainer" className="mb-4">
          <Image id="stationImage" className="rounded"
                 src={`https://proxy.zeno.fm/content/stations/${station.key}/image/?resize=500x500&v=1`}
                 height={270}
                 width={270}
                 alt={station.name}
          />
        </div>

        <div className="font-bold">{nowPlaying.title}</div>
        <div className="text-sm">{nowPlaying.artist}</div>

        <div className="flex mt-8">
          <div className="p-4 bg-light-gray flex-grow-0 mx-auto text-dark-gray rounded-full flex items-stretch flex-row items-center">

            <div className="border-0 border-r">
              {player.playing
                ? <button className="focus:outline-none h-full w-20 text-center">
                  <FiPause onClick={onPause} className="mx-auto text-2xl"/>
                </button>
                : <button className="focus:outline-none h-full w-20 text-center">
                  <FiPlay onClick={onPlay} className="mx-auto text-2xl"/>
                </button>
              }
            </div>

            <div className="w-20 text-2xl">
              <ShareModal />
            </div>

          </div>
        </div>
      </div>

      <div>

      </div>
    </div>
  )
}

export default StationInfo