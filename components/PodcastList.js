const PodcastList = ({podcasts, selectedPodcast, onSelectPodcast}) => {
  return (
    <div className="relative bg-light-gray">
      <div className="container max-w-screen-lg mx-auto py-4">

        <div className="relative overflow-x-scroll overflow-x-hidden">

          <div className="flex flex-nowrap">
            {podcasts.map((podcast) => (
              <div key={podcast.key} className="mr-4">

                <div
                  className={"inline-block whitespace-nowrap cursor-pointer py-2 px-4 rounded-full border border-1 border-gray-300 text-xs hover:bg-white" + (selectedPodcast.key === podcast.key ? " bg-white" : "")}
                  onClick={() => onSelectPodcast(podcast)}>
                  {podcast.title}
                </div>
              </div>
            ))}
          </div>
        </div>

      </div>
    </div>
  );
};

export default PodcastList;