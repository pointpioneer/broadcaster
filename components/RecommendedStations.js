import Link from "next/link";
import Image from "next/image";
import {FiMapPin, FiGlobe} from "react-icons/fi";

const RecommendedStations = (props) => {

  return (
    <div className={'container mx-auto max-w-screen-lg my-8 px-6'}>
      <h2 className="text-2xl mb-8 sm:text-left text-center ">Similar Stations</h2>

      <div className="grid sm:grid-cols-4 grid-cols-2 gap-4 ">

        {props.recommended.map((station) => (
          <div key={station.pretty_url}>
            <Link href={`/stations/${station.pretty_url}`}>
              <a>
                <Image className={"rounded"}
                       src={`https://proxy.zeno.fm/content/stations/${station.objectID}/image/?resize=500x500&v=1`}
                       height={270}
                       width={270}
                       alt={station.name}
                />
              </a>
            </Link>

            <br/>

            <div className="text-2xl text-center">
              <Link href={`/stations/${station.pretty_url}`}>
                <a>
                  {station.station_name}
                </a>
              </Link>

              <div className="text-sm mt-4 text-center">
                <div className="flex">
                  <a href="" className="flex flex-row items-center mx-auto">
                    <FiMapPin /> <span className="ml-1 font-bold">{station.city ? station.city + ', ' : ''} {station.country}</span>
                  </a>
                </div>

                {station.language &&
                <div className="flex">
                  <a href="" className="flex flex-row items-center mx-auto">
                    <FiGlobe/> <span className="ml-1 ml-2">{station.language}</span>
                  </a>
                </div>
                }
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default RecommendedStations