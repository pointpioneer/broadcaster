import {FiMapPin, FiHash, FiGlobe} from "react-icons/fi";

import Image from "next/image";
import CardSupportDonations from "./StationCards/CardSupportDonations";
import React from "react";
import CardSocialMedia from "./StationCards/CardSocialMedia";
import CardAnnouncements from "./StationCards/CardAnnouncements";
import CardWebsite from "./StationCards/CardWebsite";
import CardAppStore from "./StationCards/CardAppStore";
import CardAbout from "./StationCards/CardAbout";

const StationCards = ({station, stationCards}) => {

  return (
    <div className="bg-light-gray">
      <div className="container max-w-screen-lg mx-auto sm:py-8 p-4">

        <CardAbout station={station} card={stationCards.CardAbout}/>

        <div className="box-border mx-auto md:masonry before:box-inherit after:box-inherit">
          {
            (typeof stationCards.CardSupportDonations.stripeData != "undefined" ||
              typeof stationCards.CardSupportDonations.paypalUrl != "undefined" ||
              typeof stationCards.CardSupportDonations.cashappUrl != "undefined") &&
                <CardSupportDonations station={station} stationCards={stationCards} />
          }

          {
            typeof stationCards.CardWebsite.name != "undefined" &&
            <CardWebsite station={station} card={stationCards.CardWebsite}/>
          }

          {
            (
              typeof stationCards.CardSocialMedia.facebookUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.twitterUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.instagramUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.snapchatUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.youtubeUrl != "undefined" ||
              typeof stationCards.CardSocialMedia.facebookUrl != "undefined"
            ) &&
              <CardSocialMedia station={station} stationCards={stationCards}/>
          }

          <CardAppStore station={station} card={stationCards.CardAppStore}/>

          {
            stationCards.CardAnnouncement.announcements.length > 0 &&
              <CardAnnouncements station={station} card={stationCards.CardAnnouncement}/>
          }
        </div>
      </div>
    </div>
  )

}

export default StationCards