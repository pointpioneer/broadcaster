import Image from "next/image";
import React, {useState} from "react";
import {
  FiPause,
  FiPlay,
  FiVolume2,
  FiVolumeX,
} from "react-icons/fi";
import ShareModal from "./ShareModal";

const RadioPlayer = ({station, player, onPlay, onPause, nowPlaying}) => {

  let [volume, setVolume] = useState(50);

  const handleChange = (event) => {
    setVolume(event.target.value);
    const playerElement = document.getElementById("radioPlayer")
    playerElement.volume = event.target.value / 100
  }

  const mute = () => {
    setVolume(0);
    const playerElement = document.getElementById("radioPlayer")
    playerElement.volume = 0
  }

  const unmute = () => {
    setVolume(50);
    const playerElement = document.getElementById("radioPlayer")
    playerElement.volume = 0.5
  }

  return (
    <div className="h-16 bg-light-gray w-full border-0 border-t">

      <audio id="radioPlayer" className="hidden" />

      <div className="flex">

        <div className="flex-none">

          <div className="flex">
            <div className="flex h-16 sm:w-18 w-12 flex-row items-center">

              {player.playing
                ? <button className="focus:outline-none h-full mx-auto text-center">
                    <FiPause onClick={onPause} className="mx-auto text-2xl"/>
                  </button>
                : <button className="focus:outline-none h-full mx-auto text-center">
                    <FiPlay onClick={onPlay} className="mx-auto text-2xl"/>
                  </button>
              }

            </div>
          </div>

        </div>

        <div className="flex flex-grow truncate">

            <div className="flex flex-row items-center mx-auto">
              <div className="sm:flex hidden mr-4">
                <Image
                  src={`https://proxy.zeno.fm/content/stations/${station.key}/image/?resize=62x62&v=1`}
                  alt={station.name}
                  width={62}
                  height={62}
                />
              </div>

              <div className="text-sm">
                <div className="text-primary" title={(nowPlaying.artist ? nowPlaying.artist + ' -' : '') + nowPlaying.title}>
                  {nowPlaying.artist && nowPlaying.artist + ' -' } {nowPlaying.title}
                </div>

                <div className="font-bold" title={station.name}>
                  {station.name}
                </div>
              </div>
          </div>

        </div>

        <div className="flex-none flex flex-row items-center">

          <div className="flex ml-auto">

            <div className="flex">
              <input className="sm:flex hidden" type="range" id="volume" name="volume" min="0" max="100" value={volume} step="1" onChange={handleChange}/>

              {
                volume > 0 ?
                  (
                    <button className="focus:outline-none" onClick={mute}>
                      <FiVolume2 className="mx-2"/>
                    </button>
                  ) :
                  (
                    <button className="focus:outline-none" onClick={unmute}>
                      <FiVolumeX className="mx-2"/>
                    </button>
                  )
              }
            </div>

            <div className="mx-2">
              <ShareModal />
            </div>
          </div>
        </div>

      </div>
    </div>
  )

};

export default RadioPlayer;