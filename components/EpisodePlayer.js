import Image from "next/image";
import {
  FiPause,
  FiPlay,
  FiVolumeX,
  FiSkipBack, FiSkipForward, FiVolume2,
} from "react-icons/fi";
import ShareModal from "./ShareModal";
import React, {useState} from "react";

const EpisodePlayer = ({episodes, player, onPlay, onPause}) => {

  let [volume, setVolume] = useState(50);

  const handleChange = (event) => {
    setVolume(event.target.value);
    const playerElement = document.getElementById("episodePlayer")
    playerElement.volume = event.target.value / 100
  }

  const mute = () => {
    setVolume(0);
    const playerElement = document.getElementById("episodePlayer")
    playerElement.volume = 0
  }

  const unmute = () => {
    setVolume(50);
    const playerElement = document.getElementById("episodePlayer")
    playerElement.volume = 0.5
  }

  const getCurrentEpisodeIndex = () => {
    return episodes.map(episode => episode.key)
      .indexOf(player.episode.key)
  }

  const skip = (type) => {
    const episodeNum = getCurrentEpisodeIndex()

    let nextEpisodeNum = type === "next" ? episodeNum + 1 : episodeNum - 1

    if (nextEpisodeNum >= episodes.length) {
      nextEpisodeNum = 0
    }

    if (nextEpisodeNum < 0) {
      nextEpisodeNum = episodes.length - 1
    }

    onPlay(episodes[nextEpisodeNum])
  }

  return (
    <div className="h-16 bg-light-gray w-full border-0 border-t">

      <audio id="episodePlayer" className="hidden" />

      {
        player.episode &&
        <div className="flex">

          <div className="flex-none">

            <div className="flex">
              <div className="flex h-16 sm:w-18 flex-row items-center">

                <button className="focus:outline-none" onClick={() => skip('prev')}>
                  <FiSkipBack className="w-12"/>
                </button>

                {player.playing
                  ? <button onClick={() => onPause(player.episode)} className="focus:outline-none h-full sm:w-20 w-auto text-center">
                    <FiPause className="mx-auto text-2xl"/>
                  </button>
                  : <button onClick={() => onPlay(player.episode)} className="focus:outline-none h-full sm:w-20 w-auto text-center">
                    <FiPlay className="mx-auto text-2xl"/>
                  </button>
                }

                <button className="focus:outline-none" onClick={() => skip('next')}>
                  <FiSkipForward className="w-12"/>
                </button>

              </div>
            </div>

          </div>

          <div className="flex flex-grow truncate">

            <div className="flex flex-row items-center mx-auto">
              <div className="sm:flex hidden mr-4">
                <Image
                  src={player.episode.image}
                  alt={player.episode.title}
                  width={62}
                  height={62}
                />
              </div>

              <div className="text-sm">
                <div className="text-primary" title={player.episode.title}>
                  {player.episode.title}
                </div>

                <div className="font-bold" title={player.podcast.title}>
                  {player.podcast.title}
                </div>
              </div>
            </div>

          </div>

          <div className="flex-none flex flex-row items-center">

            <div className="flex ml-auto">

              <div className="flex">
                <input className="sm:flex hidden" type="range" id="volume" name="volume" min="0" max="100" value={volume} step="1" onChange={handleChange}/>

                {
                  volume > 0 ?
                    (
                      <button className="focus:outline-none" onClick={mute}>
                        <FiVolume2 className="mx-2"/>
                      </button>
                    ) :
                    (
                      <button className="focus:outline-none" onClick={unmute}>
                        <FiVolumeX className="mx-2"/>
                      </button>
                    )
                }
              </div>

              <div className="mx-2">
                <ShareModal />
              </div>
            </div>
          </div>

        </div>
      }

    </div>
  )
};

export default EpisodePlayer