import Link from "next/link";
import {FiPlus, FiUser} from "react-icons/fi";

const Header = () => {

  return (
    <header className="bg-secondary py-5 px-6 sm:px-12 flex flex-row items-center text-white">
      <Link href={`/`}>
        <a className="text-2xl text-white mr-8 hover:text-opacity-80">
          <svg width="68" height="16" viewBox="0 0 68 16" xmlns="http://www.w3.org/2000/svg">
            <g className="animate-fill" fill="#FEFEFE">
              <path
                d="M50.653 2.073A1.845 1.845 0 1 0 48.35 3.86v9.049L36.415.972v11.252a1.845 1.845 0 1 0 .912 0V3.175l11.936 11.936V3.86a1.845 1.845 0 0 0 1.39-1.787M13.782 11.936c-.862 0-1.583.591-1.786 1.39h-9.05L14.883 1.388H3.63a1.845 1.845 0 1 0 0 .912h9.05L.743 14.238h11.252a1.844 1.844 0 1 0 1.786-2.302M60.454 14.815a6.78 6.78 0 0 1-6.773-6.774 6.78 6.78 0 0 1 6.773-6.772 6.78 6.78 0 0 1 6.773 6.772 6.78 6.78 0 0 1-6.773 6.774m0-12.393a5.626 5.626 0 0 0-5.62 5.62 5.626 5.626 0 0 0 5.62 5.62 5.626 5.626 0 0 0 5.62-5.62 5.626 5.626 0 0 0-5.62-5.62M19.952 14.238h11.275v-.942H19.952v.942zm0-11.907h11.275v-.942H19.952v.942zm0 5.954h11.275v-.942H19.952v.942z"/>
            </g>
          </svg>
        </a>
      </Link>

      <nav className="flex flex-row items-center sm:flex hidden">
        <a href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/registrations?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
           className="flex flex-row items-center font-semibold hover:text-primary" target="_blank" rel="noopener">
          <FiPlus className="mr-1 text-primary text-xl" />
          Create Station
        </a>
        <span className="mx-2">or</span>
        <a href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/registrations?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
           className="font-semibold mr-8 hover:text-primary" target="_blank" rel="noopener">
          Podcast
        </a>

        <a href="/search" className="font-semibold hover:text-primary">
          Discovery
        </a>
      </nav>

      <nav className="ml-auto flex flex-row items-center sm:flex hidden">
        <a target="_blank" rel="noopener" href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/auth?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
           className="font-semibold hover:text-primary flex flex-row items-center mr-4">
          <FiUser className="mr-2"/>
          Log in
        </a>

        <a target="_blank" rel="noopener" href="https://tools.zeno.fm/auth/realms/broadcasters/protocol/openid-connect/registrations?client_id=zeno-tools&redirect_uri=https%3A%2F%2Ftools.zeno.fm%2F&response_mode=fragment&response_type=code&scope=openid"
           className="font-semibold py-2 px-6 bg-white rounded-full text-black hover:bg-primary">
          Sign up
        </a>
      </nav>
    </header>
  )

}

export default Header