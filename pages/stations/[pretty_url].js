import { FiPlus, FiUser } from "react-icons/fi";
import Link from 'next/link'
import Head from 'next/head'
import {getStation, getRecommendedStations, getNowPlayingData, getStationCards} from "../../lib/stations";
import {getStationPodcasts, getPodcastEpisodes} from "../../lib/podcasts";
import React, {useState, useEffect} from "react";

import RecommendedStations from '../../components/RecommendedStations'
import StationInfo from '../../components/StationInfo'
import StationCards from "../../components/StationCards";
import Footer from "../../components/Footer";

import RadioPlayer from "../../components/RadioPlayer";
import PodcastList from "../../components/PodcastList";
import EpisodeList from "../../components/EpisodeList";
import EpisodePlayer from "../../components/EpisodePlayer";
import Header from "../../components/Header";

export default function Broadcaster({station, stationCards, podcasts, recommendedStationsList}) {

  let [broadcasting, setBroadcasting] = useState('radio');

  let [nowPlaying, setNowPlaying] = useState({
    artist: '',
    title: ''
  });

  let [radioPlayer, setRadioPlayer] = useState({
    active: false,
    playing: false
  });

  const radioPlay = () => {
    if (episodePlayer.episode) {
      episodeStop()
    }

    setRadioPlayer({
      active: true,
      playing: true
    })

    const playerElement = document.getElementById("radioPlayer");

    if (playerElement.src !== station.stream_url) {
      playerElement.src = station.stream_url;
      playerElement.load();
    }
    playerElement.volume = 0.5
    playerElement.play();
  }

  const radioPause = () => {
    setRadioPlayer({
      active: true,
      playing: false
    })

    const playerElement = document.getElementById("radioPlayer");
    playerElement.pause();
  }

  const radioStop = () => {
    setRadioPlayer({
      active: false,
      playing: false
    })

    const playerElement = document.getElementById("radioPlayer");
    playerElement.pause();
    playerElement.src = ''
  }

  const [selectedPodcast, setSelectedPodcast] = useState(podcasts[0])

  const [episodes, setEpisodes] = useState([]);

  const selectPodcast = (podcast) => {
    setSelectedPodcast(podcast)

    getPodcastEpisodes(podcast.key)
      .then(episodes => {
        setEpisodes(episodes);
      });
  }

  let [episodePlayer, setEpisodePlayer] = useState({
    episode: null,
    podcast: null,
    active: false,
    playing: false
  });

  const episodePlay = (episode) => {
    radioStop()

    setEpisodePlayer({
      episode: episode,
      podcast: selectedPodcast,
      active: true,
      playing: true
    })

    const playerElement = document.getElementById("episodePlayer");

    if (playerElement.src !== episode.file_url) {
      playerElement.src = episode.file_url;
      playerElement.load();
    }
    playerElement.volume = 0.5
    playerElement.play();
  }

  const episodePause = () => {
    let player = episodePlayer
    player.playing = false

    setRadioPlayer(player)

    const playerElement = document.getElementById("episodePlayer");
    playerElement.pause();
  }

  const episodeStop = () => {
    let player = episodePlayer
    player.playing = false
    player.active = false

    setRadioPlayer(player)

    const playerElement = document.getElementById("episodePlayer");
    playerElement.pause();
    playerElement.src = ''
  }

  useEffect(() => {
    if (podcasts.length) {
      getPodcastEpisodes(podcasts[0].key)
        .then(episodes => {
          setEpisodes(episodes);
        });
    }
  }, [podcasts])

  useEffect(() => {
    const backgroundCover = document.getElementById('backgroundCover');
    backgroundCover.style.backgroundImage = `url('https://proxy.zeno.fm/content/stations/${station.key}/microsite/background_image/')`;

    // const fac = new FastAverageColor();
    //
    // fac.getColorAsync(`https://proxy.zeno.fm/content/stations/${station.key}/image/?resize=500x500&v=1`).then(color => {
    //   const container = document.getElementById('stationInfo');
    //   const backgroundCover = document.getElementById('backgroundCover');
    //   backgroundCover.style.backgroundColor = color.rgba;
    //   container.style.color = color.isDark ? '#fff' : '#000';
    // });
  }, [])

  useEffect(() =>{
    let interval = setInterval(() => {
      getNowPlayingData(station.streamName)
        .then(data => {
          setNowPlaying({
            artist: data.artist,
            title: data.title
          })
        })
    }, (1000 * 3))

    return () => clearInterval(interval)
  })

  return (
    <>
      <Head>
        <title>Station {station.name}</title>
      </Head>

      <Header />
      <div className="container mx-auto sm:py-5 py-4 text-center sm:relative sticky top-0 z-50 bg-white shadow-lg" id="broadcasting">
        <a onClick={() => {setBroadcasting('radio'); window.scrollTo(0, 0)} } className={"sm:text-lg font-semibold mr-4 cursor-pointer pb-1" + (broadcasting === 'radio' ? " text-black border-b-2 border-primary" : ' text-gray-500')}>Radio</a>

        {podcasts.length > 0 &&
        <a onClick={() => {setBroadcasting('podcasts'); window.scrollTo(0, 0)}} className={"sm:text-lg font-semibold cursor-pointer pb-1" + (broadcasting === 'podcasts' ? " text-black border-b-2 border-primary" : ' text-gray-500 ')}>Podcasts</a>
        }
      </div>

      <div className={'relative bg-black'}>
        {/*<div style={{'backgroundImage': `url('https://proxy.zeno.fm/content/stations/${station.key}/microsite/background_image/')`}}*/}
        {/*     className={'bg-center bg-cover bg-black opacity-20 w-full h-full absolute'}></div>*/}
        <div id="backgroundCover" className={'bg-center bg-cover bg-black opacity-20 w-full h-full absolute'}></div>

            <div className={broadcasting !== 'radio' ? 'hidden' : ''}>
              <StationInfo station={station} player={radioPlayer} onPause={radioPause} onPlay={radioPlay} nowPlaying={nowPlaying}/>
            </div>

            {
              podcasts.length > 0 &&
                <div className={broadcasting !== 'podcasts' ? 'hidden' : ''}>
                  <PodcastList podcasts={podcasts} selectedPodcast={selectedPodcast} onSelectPodcast={selectPodcast}/>

                  <div className="relative container max-w-screen-lg mx-auto p-4 pb-0 text-white">
                    <h2 className="text-3xl border-0 border-b border-white pb-4 border-opacity-20">
                      {selectedPodcast.title}
                    </h2>
                  </div>

                  {
                    episodes.length &&
                    <EpisodeList episodes={episodes} player={episodePlayer} onPause={episodePause} onPlay={episodePlay}/>
                  }
                </div>
            }

        </div>

      <StationCards station={station} stationCards={stationCards}/>

      <RecommendedStations recommended={recommendedStationsList} />

      <Footer/>

      <div className={!radioPlayer.active ? 'hidden' : 'sticky bottom-0 z-50'}>
        <RadioPlayer station={station} player={radioPlayer} onPause={radioPause} onPlay={radioPlay} nowPlaying={nowPlaying}/>
      </div>

      {
        episodes.length &&
        <div className={!episodePlayer.active ? 'hidden' : 'sticky bottom-0 z-50'}>
          <EpisodePlayer episodes={episodes} player={episodePlayer} onPause={episodePause} onPlay={episodePlay}/>
        </div>
      }
    </>
  )
}

export async function getStaticProps(context) {
  try {
    const station = await getStation(context.params.pretty_url)
    const stationCards = await getStationCards(station.key)
    const podcasts = await getStationPodcasts(station.key)
    const recommendedStationsList = await getRecommendedStations(station)

    return {
      props: {
        station,
        stationCards,
        podcasts,
        recommendedStationsList
      },
      revalidate: 5
    }
  } catch(err) {
    console.log(err);

    return {
      notFound: true,
    }
  }
}

export async function getStaticPaths() {
  return {
    paths: [
      '/stations/development',
      '/stations/sampleradio',
    ],
    fallback: 'blocking',
  }
}