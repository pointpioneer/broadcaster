import Head from 'next/head'
import Link from 'next/link'
import { getStations } from '../lib/stations'

export async function getStaticProps() {
  const stations = await getStations()

  return {
    props: {
      stations
    }
  }
}

export default function Home({stations }) {
  return (
    <div className="container mx-auto">

      <ul>
        <li key="development">
          <Link href={`/stations/development`}>
            <a>development</a>
          </Link>
        </li>

        <li key="sampleradio">
          <Link href={`/stations/sampleradio`}>
            <a>sampleradio</a>
          </Link>
        </li>

        {stations.map(({ pretty_url }) => (
          <li key={pretty_url}>
            <Link href={`/stations/${pretty_url}`}>
              <a>{pretty_url}</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  )
}
