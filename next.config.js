// next.config.js
module.exports = {
  images: {
    domains: ['proxy.zeno.fm', 'podcast.zenomedia.com', 'editor.zenomedia.com', 'zenoplay.zenomedia.com'],
  },

  webpack: (config, { buildId, dev, isServer, defaultLoaders, webpack }) => {
    config.node = {
      fs: 'empty'
    }
    return config
  },
}