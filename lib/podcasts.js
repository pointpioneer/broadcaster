export const getStationPodcasts = async (stationKey) => {
  const res = await fetch(`https://zenoplay.zenomedia.com/api/zenofm/stations/${stationKey}/podcasts/`)
  return await res.json()
}

export const getPodcastEpisodes = async (podcastKey) => {
  const response = await fetch(`https://zenoplay.zenomedia.com/api/zenofm/podcasts/${podcastKey}/episodes/`)
  return await response.json();
}