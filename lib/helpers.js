export const validateParameter = (parameter) => {
  parameter = parameter.trim()

  if (!parameter || parameter === 'Unknown' || parameter === 'None' || parameter === '-') {
    return ''
  }

  return parameter
}