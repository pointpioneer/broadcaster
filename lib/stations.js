import {validateParameter} from './helpers'

export async function getStations() {
  const res = await fetch(`https://zenoplay.zenomedia.com/api/zenofm/sitemaps/stations`)
  const data = await res.json()

  return data.slice(0, 100)
}

const getCategoryTitle = stationCategoryId => {
  const stationCategories = {
    "Sports": 32,
    "NewsSports": 35,
    "General": 38,
    "Music": 30,
    "CollegeUniversity": 39,
    "News": 31,
    "Comedy": 37,
    "Religious": 33,
    "Children": 36,
    "Talk": 34
  }

  let category = Object.keys(stationCategories).find(key => stationCategories[key] === stationCategoryId);

  if (typeof category === "undefined") {
    category = ''
  }

  return category
}

export async function getRecommendedStations(station) {
  let filters = {}

  if (station.country) {
    filters.country = [station.country]
  } else if (station.category) {
    filters.genre = [station.category]
  }

  let data = {
    query: '',
    page: 1,
    hitsPerPage: 5,
    filters: filters
  }

  const res = await fetch('https://zenoplay.zenomedia.com/api/zenofm/search/stations', {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });

  const recommended = await res.json();

  return recommended.hits.slice(0, 4)
}

export async function getStation(pretty_url) {
  try {
    let res = await fetch(`https://proxy.zeno.fm/api/stations/?pretty_url=${pretty_url}`)
    const stationData = await res.json()

    let station = {
      key: stationData.key,
      name: stationData.name,
      pretty_url: stationData.pretty_url,
      country: stationData.country,
      stream_url: `https://stream.zeno.fm/${stationData.streamname}`,
      streamName: stationData.streamname,
    }

    res = await fetch(`https://proxy.zeno.fm/api/stations/${station.key}/profile`)
    let profileData = await res.json()

    station.description = profileData.description

    station.city = station.city ? validateParameter(profileData.city) : ''

    station.social = profileData.social_contact_info
    station.apps = profileData.apps
    station.languages = profileData.language
    station.category = getCategoryTitle(profileData.station_category)

    return station
  } catch(err) {
    return null
  }
}

export const getNowPlayingData = async (streamName) => {
  const response = await fetch(`https://nowplaying-dot-zeno-stats.appspot.com/api/stations/${streamName}/now_playing/`)
  return await response.json();
}

export const getStationCards = async (stationKey) => {
  const response = await fetch(`https://editor.zenomedia.com/api/zenofm/${stationKey}/dashboardCards`)
  const cardsData = await response.json();

  let cards = {};

  cardsData.forEach(card => {
    cards[card.type] =  card
  })

  cards.CardAnnouncement.announcements= cards.CardAnnouncement.announcements.slice(0, 3)

  return cards
}