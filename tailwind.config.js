// tailwind.config.js
const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'primary': '#F5AB19',
        'secondary': '#212124',
        'light-primary': '#fca801',
        'light-secondary': '#101011',
        'light-gray': '#F3F3F3',
        'medium-gray': '#E6E6E6',
        'dark-gray': '#1D1D1F',


        'facebook': '#4267B2',
        'twitter': '#1DA1F2',

        'app': '#28282A',
        'stripe': '#635bff',
        'paypal': '#0070ba',
        'cashapp': '#00d64f',
      },

      borderColor: {
        'primary': '#F5AB19',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
